import java.math.*;

public final class Fraction {

    private final double numerateur;
    private final double denominateur;


    public Fraction(double numerateur, double denominateur) {
        this.numerateur = numerateur;
        this.denominateur = denominateur;
    }

    public Fraction(double numerateur) {
        this.numerateur = numerateur;
        this.denominateur = 1;
    }

    public Fraction() {
        this.numerateur = 0;
        this.denominateur = 1;
    }

    public static final Fraction ZERO = new Fraction();
    public static final Fraction UN = new Fraction(1, 1);

    public double getNumerateur() {
        return numerateur;
    }

    public double getDenominateur() {
        return denominateur;
    }

    public double virguleFLottante() {
        return this.numerateur / this.denominateur;
    }

    public Fraction somme(Fraction F) {

        return new Fraction(this.numerateur * F.getDenominateur() + F.getNumerateur(),
                this.denominateur * F.getDenominateur());
    }

    public boolean egalite(Fraction F) {
        if (this.virguleFLottante() == F.virguleFLottante()) {
            return true;
        }
        return false;
    }

    public String ToString() {
        return new String("numerateur = " + this.getNumerateur() + ", denominateur = " + this.getDenominateur());
    }

    public String comparaison(Fraction F) {
        if (this.egalite(F)) return "egal";
        else if (this.virguleFLottante() < F.virguleFLottante()) return "superieur";
        else return "inferieur";
    }
}